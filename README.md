You will need:
- An AWS account - services will incur a small cost - just remember to close down when not in use#
- Create an Amazon Workspace - [here](https://aws.amazon.com/workspaces/?trk=ps_a134p000006vmXVAAY&trkCampaign=acq_paid_search_brand&sc_channel=PS&sc_campaign=acquisition_UKIR&sc_publisher=Google&sc_category=EUC&sc_country=UKIR&sc_geo=EMEA&sc_outcome=acq&sc_detail=amazon%20workspaces&sc_content=Workspaces_e&sc_matchtype=e&sc_segment=514416664959&sc_medium=ACQ-P|PS-GO|Brand|Desktop|SU|EUC|WorkSpaces|UKIR|EN|Text&s_kwcid=AL!4422!3!514416664959!e!!g!!amazon%20workspaces&ef_id=Cj0KCQiA5OuNBhCRARIsACgaiqUFwZCOpgXFkcFupEtZ3NzH7QNT-oriQb2rFe-SlVN3xLGb6kRJxhAaArzhEALw_wcB:G:s&s_kwcid=AL!4422!3!514416664959!e!!g!!amazon%20workspaces)
- Log onto your workspace follow instructions below:

1. Download into AWS_Video
2. cd into AWS_Video
3. configure your aws credentials and decide on a unique id for your bucket
4. edit makevideo.bat to reference your aws region and unique id for s3
5. run ./makevideo.bat
6. the demo video will be ingested, and translations provided for each language specified in the bat file
7. architecture
<img src="Architecture.gif" align="center" width="800" alt="Architecture">

What does it do?

Explores AWS Transcribe (audio to text), AWS Translate (textual language translation) and AWS Polly (text to speech)

I have provided a short sample video - demo.mp4
When you run makevideo.bat the video is run through the AWS Transcribe service to extract the audio to English text. The text is then translated to a number of languages as specified in the bat file. The transcripts are then converted to audio and also made available as subtitles. The resulting audio and transcript are merged into new lannguage specific video snippets.

Once this is done, you can run concatenateVideos.py to pull all the snippets into one video.

What it lacks?

- Language translations, and voices, are at a different speed in different languages
- You will see there is hard coding in concantenateVideos.py to make the durations match
- Actually, the results of transcribe/translate return start and ends for each transcribed/translated word. This should be dynamic and may even be able to lip synch (ish!) with the video
- How would you make this serverless? For instance, new short video uploaded to an S3 bucket triggers a Lambda function starts a workflow to run the services and combine the results (clue - it will require quite a few triggers)
- What controls could you put in place to prevent large videos being processed - what's your spending cap?
