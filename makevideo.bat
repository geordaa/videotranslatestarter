REM ==================================================================================
REM 
REM Purpose: This batchfile invokes the translatevideo.py file with parameters 
REM
REM ==================================================================================

cls
python translatevideo.py -region {your_region} -inbucket {your_unique_id}-aiml-test/ -infile demo.mp4 -outbucket {your_unique_id}-aiml-test/ -outfilename subtitledVideo -outfiletype mp4 -outlang es de fr it en
python concatenatevideos.py
